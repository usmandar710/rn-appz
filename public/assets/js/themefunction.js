/*--------------------------------------
		CUSTOM FUNCTION WRITE HERE		
--------------------------------------*/
"use strict";
jQuery(function() {
	/* -------------------------------------
			THEME ACCORDION
	-------------------------------------- */
	function themeAccordion() {
		jQuery('.at-panelcontent').hide();
		jQuery('.at-accordion h4').on('click',function() {
			if(jQuery(this).next().is(':hidden')) {
				jQuery('.at-accordion h4').removeClass('active').next().slideUp('slow');
				jQuery(this).toggleClass('active').next().slideDown('slow');
			}
		});
	}
	themeAccordion();
	/* -------------------------------------
			TinyMce Editor
	-------------------------------------- */
	if (jQuery('#at-tinymceeditor').length > 0) {
		tinymce.init({
			selector: 'textarea#at-tinymceeditor',
			height: 250,
			theme: 'modern',
			plugins: ['advlist autolink lists link image charmap print preview hr anchor pagebreak', 'searchreplace wordcount visualblocks visualchars code fullscreen', 'insertdatetime media nonbreaking save table contextmenu directionality', 'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'],
			toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
			toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
			image_advtab: true,
		});
	}
	/*--------------------------------------
			SHOW HIDE PROFILE			
	--------------------------------------*/
	jQuery('#at-btnaddcard').on('click', function(event) {
		event.preventDefault();
		jQuery('#at-dashboard').fadeOut('300', function(){
			jQuery('#at-addcard').fadeIn('300');
		});
	});
	jQuery('#at-btnaddcarddetail').on('click', function(event) {
		event.preventDefault();
		jQuery('#at-addtittleholder').fadeOut('300', function(){
			jQuery('#at-addcarddetail').fadeIn('300');
		});
	});
	jQuery('#at-btnaddcarddetailvtwo').on('click', function(event) {
		event.preventDefault();
		jQuery('#at-addcarddetail').fadeOut('300', function(){
			jQuery('#at-addcarddetailtwo').fadeIn('300');
		});
	});
	jQuery('#at-btnsubmit').on('click', function(event) {
		event.preventDefault();
		jQuery('#at-addcarddetailtwo').fadeOut('300', function(){
			jQuery('#at-dashboard').fadeIn('300');
		});
	});
	$('#at-btnback').on('click', function(event) {
		event.preventDefault();
		$('#at-addcarddetailtwo').fadeOut('300', function(){
			$('#at-addcarddetail').fadeIn('300');
		});
	});
	$('#at-btnbacktwo').on('click', function(event) {
		event.preventDefault();
		$('#at-addcarddetail').fadeOut('300', function(){
			$('#at-addtittleholder').fadeIn('300');
		});
	});






});