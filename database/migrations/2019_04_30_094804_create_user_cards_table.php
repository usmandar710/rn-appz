<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_cards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('title');
            $table->text('text');
            $table->string('photo_id')->nullable();
            $table->string('surgeon_name')->nullable();
            $table->string('procedure_type')->nullable();
            $table->string('special_need')->nullable();
            $table->string('suplies')->nullable();
            $table->string('drug')->nullable();
            $table->string('equipment')->nullable();
            $table->string('instruments')->nullable();
            $table->string('nursing_Instruction')->nullable();
            $table->string('positioning')->nullable();
            $table->string('scrub_hint')->nullable();
            $table->string('others')->nullable();
            $table->string('checklist')->nullable();
            $table->text('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_cards');
    }
}
