<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Role::create([
            'name' => 'admin',
        ]);
        \App\Role::create([
            'name' => 'user',
        ]);

        \App\User::create([

            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'contact' => '03334445555',
            'organization_id' => 0,
            'job_id' => 0,
            'role_id' => 1,
            'dob' => \Carbon\Carbon::now()->toDateString(),
            'password' => bcrypt(123123),
        ]);
    }
}
