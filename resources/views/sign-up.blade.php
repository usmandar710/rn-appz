<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->	<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>RN-APPZ  | Sign up</title>
    <meta name="description" content="">
    <link rel="icon" href="{{asset('favicon.png')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('layouts.css')
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!--************************************
        Login Start
*************************************-->
<div id="at-loginarea" class="at-loginarea">
    <div class="at-signupcontentarea">
        <!-- <figure class="at-loginlogo">
            <img src="images/logo.jpg" alt="Logo Image">
        </figure> -->
        <form class="at-formtheme at-formsignup" method="post" action="{{route('user-register')}}">
            {{csrf_field()}}
            <fieldset>
                <legend>Sing up</legend>
                <div class="form-group">
                    <label>Full Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Full Name" autocomplete="off" required>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" placeholder="Email"  required>
                </div>
                <div class="form-group">
                    <label>Contact</label>
                    <input type="text" name="contact" class="form-control" placeholder="Contact" autocomplete="off" required>
                </div>
                <div class="form-group">
                    <label>Organization</label>
                    <span class="at-select">
                        <select name="organization" required>
                            <option value="">Select</option>
                            @forelse($organizations as $key => $organ)
                                <option value="{{$organ->id}}">{{$organ->name}}</option>
                            @empty
                            @endforelse
                        </select>
                    </span>
                </div>

                <div class="form-group">
                    <label>Job</label>
                    <span class="at-select">
                        <select name="job" required>
                            <option value="">Select</option>
                            @forelse($jobs as $key => $job)
                                <option value="{{$job->id}}">{{$job->name}}</option>
                            @empty
                            @endforelse
                        </select>
                    </span>
                </div>

                <div class="form-group">
                    <label>Date of Birth</label>
                    <input type="date" name="dob" class="form-control" placeholder="Date Of Birth" autocomplete="off" required>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" autocomplete="off" required>
                </div>
                <div class="form-group">
                    <label>Confirm Password</label>
                    <input type="password" name="password_confirmation" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Confirm Password" autocomplete="off" required>
                </div>
                <div class="at-btnarea">
                    <button type="submit" class="at-btn">Sign up</button>
                </div>
                <spam class="at-alreadymemeber">Already a member? <a href="{{url('/login')}}">Login</a></spam>
            </fieldset>
        </form>
    </div>
</div>
<!--************************************
        Login End
*************************************-->
@include('layouts.js')
</body>
</html>