@extends('layouts.master')
@section('body')
    <div class="at-adddepartmentcontent">
        <div class="at-themetableholder">
            @if(count($users))
                <table class="table at-themetable at-tableadddepartment">
                    <thead>
                    <tr>
                        <th>Sr.</th>
                        <th>User Name</th>
                        <th>User Email</th>
{{--                        <th>Action</th>--}}
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($users as $key => $user)
                        <tr>
                            <td data-title="Sr."><span>{{$key+1}}</span></td>
                            <td data-title="Department Name">
                                <h3>{{$user->name}} </h3>
                            </td>
                            <td><h3>{{$user->email}}</h3></td>
{{--                            <td data-title="Action">--}}
{{--                                <ul class="at-btnactions">--}}
{{--                                    <li><a href="{{route('edit-organization',$user->id)}}">edit</a></li>--}}
{{--                                    <li>--}}
{{--                                        <form action="{{route('delete-organization',$user->id)}}" method="post">--}}
{{--                                            <input type="hidden" name="_method" value="DELETE">--}}
{{--                                            @method('DELETE')--}}
{{--                                            @csrf--}}
{{--                                            <button type="submit"  onclick="return confirm('Are you sure you want to delete this Organization?');">delete</button>--}}
{{--                                        </form>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </td>--}}
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            @endif

        </div>
    </div>


@endsection
@section('scripts')
    <script>
        $(document).ready(function(){

            $('#organ-form').on('submit',function(e){
                e.preventDefault();
                formData = $(this).serialize();
                $.ajax({
                    url: '{{url('admin/submit-organization')}}',
                    method: 'POST',
                    _token : '{{csrf_token()}}',
                    data: formData,
                    success: function (data) {
                    },
                });
            })
        });
    </script>
@endsection