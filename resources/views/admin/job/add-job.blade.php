@extends('layouts.master')
@section('body')
    <div class="at-adddepartmentcontent">
        <div class="at-themetableholder">
            <form  class="at-formorganization" action="{{route('submit-job')}}" method="POST"  id="organ-form">
                @csrf
                <span>Job</span>
                <input type="text" name="name" >
                <button type="submit" class="at-btnuploadfile">submit</button>
            </form>
                <table class="table at-themetable at-tableadddepartment">
                    <thead>
                    <tr>
                        <th>Sr.</th>
                        <th>Job Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody class="allJobs">
                    @if(count($jobs))
                    @foreach($jobs as $key => $job)
                        <tr>
                            <td data-title="Sr."><span>{{$key+1}}</span></td>
                            <td data-title="Department Name">
                                <h3>{{$job->name}} </h3>
                            </td>
                            <td data-title="Action">
                                <ul class="at-btnactions">
                                    <li>
                                        <a class="at-editicon" href="{{route('edit-job',$job->id)}}"><i class="fa fa-edit"></i></a>
                                        <form class="at-trashform" action="{{route('delete-job',$job->id)}}" method="post">
                                            <input type="hidden" name="_method" value="DELETE">
                                            @method('DELETE')
                                            @csrf
                                            <button class="at-trashicon" type="submit"  onclick="return confirm('Are you sure you want to delete this Job?');"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>

        </div>
    </div>


@endsection
@section('scripts')
    <script>
        $(document).ready(function(){

            $('#organ-form').on('submit',function(e){
                e.preventDefault();
                formData = $(this).serialize();
                $.ajax({
                    url: '{{url('admin/submit-job')}}',
                    method: 'POST',
                    _token : '{{csrf_token()}}',
                    data: formData,
                    success: function (response) {
                        $('.allJobs').append(response);
                        },
                });
            })
        });
    </script>
@endsection