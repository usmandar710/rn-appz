<tr>
<td data-title="Sr."><span>{{$sr}}</span></td>
<td data-title="Department Name">
    <h3>{{$data->name}} </h3>
</td>
<td data-title="Action">
    <ul class="at-btnactions">
        <li>
            <a class="at-editicon" href="{{route('edit-job',$data->id)}}"><i class="fa fa-edit"></i></a>
            <form class="at-trashform" action="{{route('delete-job',$data->id)}}" method="post">
                <input type="hidden" name="_method" value="DELETE">
                @method('DELETE')
                @csrf
                <button class="at-trashicon" type="submit"  onclick="return confirm('Are you sure you want to delete this Job?');"><i class="fa fa-trash"></i></button>
            </form>
        </li>
    </ul>
</td>
</tr>