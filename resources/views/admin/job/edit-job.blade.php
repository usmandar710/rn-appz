@extends('layouts.master')
@section('body')
    <form  class="at-formedited" action="{{route('update-job',$job->id)}}" method="POST" >
        @method('put')
        @csrf
        <span>Job</span>
        <input type="text" name="name" value="{{$job->name}}" >
        <button type="submit" class="at-btnuploadfile">Update</button>
    </form>
@endsection
