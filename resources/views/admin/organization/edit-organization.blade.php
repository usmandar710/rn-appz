@extends('layouts.master')
@section('body')
    <form  class="at-formedited" action="{{route('update-organization',$organ->id)}}" method="POST" >
        @method('put')
        @csrf
        <span>organization</span>
        <input type="text" name="name" value="{{$organ->name}}" >
        <button type="submit" class="at-btnuploadfile">Update</button>
    </form>
@endsection
