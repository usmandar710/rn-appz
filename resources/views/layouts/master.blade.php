<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->	<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>RN-APPZ | Home</title>
    <meta name="description" content="">
    <link rel="icon" href="favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('layouts.css')
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!--************************************
        Wrapper Start
*************************************-->
<div id="at-wrapper" class="at-wrapper">
    <!--************************************
            Header Start
    *************************************-->
    <header id="at-header" class="at-header at-haslayout">
        <div class="at-leftarea">
            <strong class="at-logo">
                <a href="{{url('/home')}}"><img src="{{asset('assets/images/logo.jpg')}}" alt="Logo Image"></a>
            </strong>
            {{--<h1>Home</h1>--}}
        </div>
        <div class="at-rightarea">
            <form class="at-formtheme at-formsearchhead"  method="get" action="{{request()->url()}}">
                <fieldset>
                    <div class="form-group">
                        <i class="fas fa-search"></i>
                        <input type="search" name="title" class="form-control" placeholder="">
                    </div>
                </fieldset>
            </form>
            @if(\Illuminate\Support\Facades\Auth::user()->isUser())
                <a  class="at-btnaddcard" href="{{route('add-card')}}"><i class="icon-plus"></i></a>
            @else
            @endif
            {{--<div class="dropdown at-dropdown">
            <figure>
                <a href="javascript:void(0);">
                    <img src="{{asset('assets/images/logo.jpg')}}" alt="user image">
                </a>
            </figure>
            <a href="javascript:void(0);">
                <span>{{\Illuminate\Support\Facades\Auth::user()->name}}</span>
                <i class="fa fa-caret-down" aria-hidden="true"></i>
            </a>
            <ul>
                <li>
                    <a href="{{route('edit-profile',\Illuminate\Support\Facades\Auth::user()->id)}}">
                        <i class="icon-editprofile"></i>
                        <span>Edit Profile</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('logout')}}">
                        <i class="icon-logout"></i>
                        <span>LogOut</span>
                    </a>
                </li>
            </ul>
        </div>--}}

            <a class="at-btnfilters" href="javascript:void(0)"><i class="fas fa-sliders-h"></i></a>

            <div class="dropdown at-dropdown">
                <a class="at-btnsetting" href="javascript:void(0)" id="dLabel" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-cog"></i></a>
                <ul class="dropdown-menu" aria-labelledby="dLabel">
                    <li>
                        <a href="{{route('edit-profile',\Illuminate\Support\Facades\Auth::user()->id)}}">
                            <i class="icon-editprofile"></i>
                            <span>Edit Profile</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('logout')}}">
                            <i class="icon-logout"></i>
                            <span>LogOut</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <nav class="at-nav">
            <ul>
                @if(\Illuminate\Support\Facades\Auth::user()->isUser())
                    <li class="{{\Illuminate\Support\Facades\Request::is('home') == 1 ? 'active' : ''}}"><a href="{{url('/home')}}">Card</a></li>
                    <li class="{{\Illuminate\Support\Facades\Request::is('calendar') == 1 ? 'active' : ''}}"><a href="{{route('calendar')}}">Calendar</a></li>
                    <li class="{{\Illuminate\Support\Facades\Request::is('upload-file') == 1 ? 'active' : ''}}"><a href="{{route('upload-file')}}">Files</a></li>
                    <li class="{{\Illuminate\Support\Facades\Request::is('alfalfa') == 1 ? 'active' : ''}}"><a href="javascript:void(0);">Chat</a></li>
                @elseif(\Illuminate\Support\Facades\Auth::user()->isAdmin())
                    <li class="{{\Illuminate\Support\Facades\Request::is('admin/add-organization') == 1 ? 'active' : ''}}"><a href="{{url('admin/add-organization')}}">Organization</a></li>
                    <li class="{{\Illuminate\Support\Facades\Request::is('admin/add-job') == 1 ? 'active' : ''}}"><a href="{{url('admin/add-job')}}">Job Types</a></li>
                    <li class="{{\Illuminate\Support\Facades\Request::is('admin/users') == 1 ? 'active' : ''}}"><a href="{{url('admin/users')}}">Users</a></li>
                @endif
            </ul>
        </nav>
    </header>
    <!--************************************
            Header End
    *************************************-->
    <!--************************************
            Main Start
    *************************************-->
    <main id="at-main" class="at-main at-haslayout">

        @yield('body')

    </main>
    <!--************************************
            Main End
    *************************************-->
</div>
<!--************************************
        Wrapper End
*************************************-->
@include('layouts.js')
@yield('scripts')
</body>
</html>