@extends('layouts.master')
@section('body')
    <div id="at-content" class="at-content">
        <div id="at-dashboard" class="at-dashboard">
            <div class="at-contentholder">
{{--                <div class="at-contenthead">--}}
{{--                    <h2>Cards</h2>--}}
{{--                    <form class="at-formtheme at-formsearch" method="get" action="{{request()->url()}}">--}}
{{--                        <fieldset>--}}
{{--                            <div class="form-group at-inputwithicon">--}}
{{--                                <i class="fa fa-search"></i>--}}
{{--                                <input type="searc" name="title" class="form-control" placeholder="Search here...">--}}
{{--                            </div>--}}
{{--                        </fieldset>--}}
{{--                    </form>--}}
{{--                </div>--}}
                <div class="at-contentbox">
                    <div class="at-cards">
                        @forelse($cards as $card)
                            <div class="at-card">
                                <div class="at-cardhead">
                                    <h3><a href="{{route('edit-card',$card->id)}}">{{$card->title}}</a></h3>
                                </div>
                                <div class="at-description">
                                    <p>{!!  \Illuminate\Support\Str::limit($card->text, 150, $end = '.....') !!} </p>
                                    <h5>created by {{$card->user->name}}</h5>
                                </div>
                                <div class="clearfix"></div>
                                <ul class="at-btnactions">
                                    <li>
                                        <form action="{{route('delete-card',$card->id)}}" method="post">
                                            <input type="hidden" name="_method" value="DELETE">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit"  onclick="return confirm('Are you sure you want to delete this card?');"><i class="icon-delete"></i></button>
                                        </form>
                                    </li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-print"></i></a></li>
                                </ul>
                            </div>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection