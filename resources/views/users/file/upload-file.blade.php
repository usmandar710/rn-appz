@extends('layouts.master')
@section('body')
        <div class="at-adddepartmentcontent">
            <div class="at-themetableholder">
                @if(count($files))
                <table class="table at-themetable at-tableadddepartment">
                    <thead>
                    <tr>
                        <th>Sr.</th>
                        <th>File Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                        @foreach($files as $key => $file)
                            <tr>
                                <td data-title="Sr."><span>{{$key +1}}.</span></td>
                                <td data-title="Department Name">
                                    <h3>{{$file->file}} </h3>
                                </td>
                                <td data-title="Action">

{{--                                        <a href="{{ URL::to( '/exports/invoices/' . $filename)  }}" target="_blank">{{ $filename }}</a>--}}
                                    <a href="{{asset('user-files/'.$file->file)}}" target="_blank">download</a>
{{--                                        <ul class="at-btnactions">--}}
{{--                                            <li><a href="{{url('super-admin/edit-department/'.$department->id)}}"><i class="icon-edit-icon"></i></a></li>--}}
{{--                                            <li>--}}
{{--                                                <form action="{{url('super-admin/delete-department/')}}" method="post">--}}
{{--                                                    <input type="hidden" name="_method" value="DELETE">--}}
{{--                                                    @method('DELETE')--}}
{{--                                                    @csrf--}}
{{--                                                    <button type="submit"  onclick="return confirm('Are you sure you want to delete this department?');"><i class="icon-delete_icon"></i></button>--}}
{{--                                                </form>--}}
{{--                                            </li>--}}
{{--                                        </ul>--}}
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                @endif
                <form class="at-formuploadfile" action="{{route('submit-file')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <i class="fa fa-unlink"></i>
                    <span>Attach Document</span>
                    <input type="file" name="file[]" multiple>
                    <button type="submit" class="at-btnuploadfile">submit</button>
                </form>
            </div>
        </div>


@endsection