@extends('layouts.master')
@section('body')
    <div id="at-content" class="at-content">
        <div class="at-detailpage">
            <div class="at-contentholder">
                <div class="at-contenthead">
                    <h2>Card Detail</h2>
                </div>
                <div class="at-contentbox">
                    <div class="at-addcard">
                        <form class="at-formtheme at-formaddcard" method="post" action="{{route('update-card',$card->id)}}" enctype="multipart/form-data">
                            @method('PUT')
                            {{csrf_field()}}
                            <div class="at-addtittleholder">
                                <fieldset>
                                    <div class="form-group">
                                        <label>Card Title</label>
                                        <input type="text" name="title" class="form-control" value="{{$card->title}}" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Card Text</label>
                                        <textarea id="at-tinymceeditor" name="text">{{$card->text}}</textarea>
                                    </div>
{{--                                    <div class="form-group">--}}
{{--                                        <ul class="at-btnactions">--}}
{{--                                            <li>--}}
{{--                                                <div class="at-fileupload">--}}
{{--                                                    <input type="file" name="photo_id" id="at-uploadimg" >--}}
{{--                                                    <label for="at-uploadimg"><i class="icon-camera"></i></label>--}}
{{--                                                </div>--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <a href="javascript:void(0);"><i class="icon-mic"></i></a>--}}
{{--                                            </li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
                                </fieldset>
                            </div>
                            <div class="at-addcarddetail">
                                <fieldset>
                                    <div class="form-group">
                                        <label>Surgeon Name</label>
                                        <input type="text" name="surgeon-name" class="form-control" value="{{$card->surgeon_name}}" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Procedure Type</label>
                                        <input type="text" name="procedure-type" class="form-control" value="{{$card->procedure_type}}" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Special Needs</label>
                                        <input type="text" name="special-needs" class="form-control" value="{{$card->special_need}}" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Suplies</label>
                                        <input type="text" name="suplies" class="form-control" value="{{$card->suplies}}" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Drugs</label>
                                        <input type="text" name="drugs" class="form-control" value="{{$card->drug}}" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Equipment</label>
                                        <input type="text" name="equipment" class="form-control" value="{{$card->equipment}}" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Instruments</label>
                                        <input type="text" name="instruments" class="form-control" value="{{$card->instruments}}" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Nursing Instructions</label>
                                        <input type="text" name="nursing-instructions" class="form-control" value="{{$card->nursing_Instruction}}" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Positioning</label>
                                        <input type="text" name="positioning" class="form-control" value="{{$card->positioning}}" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Scrub Hints</label>
                                        <input type="text" name="scrub-hints" class="form-control" value="{{$card->scrub_hint}}" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label>Others</label>
                                        <input type="text" name="others" class="form-control" value="{{$card->others}}" autocomplete="off">
                                    </div>
                                </fieldset>
                            </div>
                            <div class="at-addcarddetailtwo">
                                <fieldset>
                                    <div id="at-accordion" class="at-accordion" role="tablist" aria-multiselectable="true">
                                        <div class="at-panel">
                                            <h4>Attachments <i class="fa fa-angle-right"></i></h4>
                                            <div class="at-panelcontent">
                                                <div class="at-checkboxes2">
                                                    @forelse($card->files as $file)
                                                        <span class="at-checkbox">
                                                            <em for="at-checktwo{{$file->id}}">{{$file->file}}</em>
                                                            <a href="{{asset('card-images/'.$file->file)}}" target="_blank">download</a>
                                                        </span>
                                                    @empty
                                                    @endforelse
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="at-addmore at-addnewlist">
                                                    <div class="form-group">
                                                        <div class="at-formuploadfile">
                                                            <input type="file" name="file[]" multiple id="at-uploadfile">
                                                            <label for="at-uploadfile">
                                                                <i class="fa fa-unlink"></i>
                                                                <span>Attach Document</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="at-accordion" class="at-accordion" role="tablist" aria-multiselectable="true">
                                        <div class="at-panel">
                                            <h4>Checklist <i class="fa fa-angle-right"></i></h4>
                                            <div class="at-panelcontent">
                                                <div class="at-checkboxes">
                                                    @forelse($checkLists as $checkList)
                                                        <span class="at-checkbox">
                                                            <input type="checkbox" name="checklist[]" id="at-checktwo{{$checkList->id}}" value="{{$checkList->id}}" {{$cardCheckList->contains('id',$checkList->id) ? 'checked' : ''}}>
                                                            <label for="at-checktwo{{$checkList->id}}">{{$checkList->name}}</label>
                                                        </span>
                                                    @empty
                                                    @endforelse
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="at-addmore at-addnewlist">
                                                    <div class="form-group">
                                                        <a href="javascript:void(0)" id="checkListBtn"><i class="fa fa-paper-plane"></i></a>
                                                        <input  class="form-control"  placeholder="Add New" id="checkListInput">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="at-panel">
                                            <h4>Comments<i class="fa fa-angle-right"></i></h4>
                                            <div class="at-panelcontent">
                                                <div class="at-description">
                                                    <textarea  name="comment">{{$card->comment}}</textarea>
                                                </div>
                                                <div class="clearfix"></div>

                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="at-btnarea">
                                <button type="submit"  class="at-btn">Save</button>
                            </div>
                        </form>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $('#checkListBtn').on('click',function () {
                var name = $('#checkListInput').val();
                $.ajax({
                    type : 'POST',
                    url : "{{route('submit-checkList')}}",
                    data : {
                        'name' : name,
                        '_token' : '{{csrf_token()}}'
                    },
                    success : function (response) {
                        var element = '<span class="at-checkbox">\n' +
                            '<input type="checkbox" name="checklist[]" id="at-checktwo'+response.id+'" value="'+response.id+'">\n' +
                            '<label for="at-checktwo'+response.id+'">'+response.name+'</label>\n' +
                            '</span>';
                        $('.at-checkboxes').append(element);
                        $('#checkListInput').val('');

                    }



                })

            });


        })
    </script>
@endsection