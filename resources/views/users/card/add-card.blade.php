@extends('layouts.master')
@section('body')
    <div class="at-addcard">
        <form class="at-formtheme at-formaddcard" id="card_form" method="post" action="{{route('submit-card')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div id="at-addtittleholder" class="at-contentholder at-addtittleholder ">
                <div class="at-contenthead">
                    <h2>Add Card</h2>
                </div>
                <div class="at-contentbox">
                    <fieldset>
                        <div class="form-group">
                            <label>Card Title</label>
                            <input type="text" name="title" id="title" class="form-control" value="{{old('title')}}" placeholder="Type Card Title" autocomplete="off" required>
                        </div>
                        <div class="form-group">
                            <label>Card Text</label>
                            <textarea name="text" id="at-tinymceeditor" required></textarea>
                        </div>
                        <div class="form-group">
                            <ul class="at-btnactions">
                                <li>
                                    <div class="at-fileupload">
                                        <input type="file" name="photo_id[]" multiple id="at-uploadimg">
                                        <label for="at-uploadimg"><i class="icon-camera"></i></label>
                                    </div>
                                </li>
                                <li>
                                    <a href="javascript:void(0);"><i class="icon-mic"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="at-btnarea">
                            <a id="at-btnaddcarddetail" class="at-btn" href="javascript:void(0);">Next</a>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div id="at-addcarddetail" class="at-contentholder at-addcarddetail">
                <div class="at-contenthead">
                    <a id="at-btnbacktwo" href="javascript:void(0);" class="at-btnback"><i class="icon-arrow-left"></i></a>
                    <h2>Add Card</h2>
                </div>
                <div class="at-contentbox">
                    <fieldset>
                        <div class="form-group">
                            <label>Surgeon Name</label>
                            <input type="text" name="surgeon-name" class="form-control" placeholder="Type Surgeon Name" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>Procedure Type</label>
                            <input type="text" name="procedure-type" class="form-control" placeholder="Procedure Type" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>Special Needs</label>
                            <input type="text" name="special-needs" class="form-control" placeholder="Special Needs" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>Suplies</label>
                            <input type="text" name="suplies" class="form-control" placeholder="Suplies" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>Drugs</label>
                            <input type="text" name="drugs" class="form-control" placeholder="Drugs" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>Equipment</label>
                            <input type="text" name="equipment" class="form-control" placeholder="Equipment" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>Instruments</label>
                            <input type="text" name="instruments" class="form-control" placeholder="Instruments" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>Nursing Instructions</label>
                            <input type="text" name="nursing-instructions" class="form-control" placeholder="Nursing Instructions" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>Positioning</label>
                            <input type="text" name="positioning" class="form-control" placeholder="Positioning" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>Scrub Hints</label>
                            <input type="text" name="scrub-hints" class="form-control" placeholder="Scrub Hints" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>Others</label>
                            <input type="text" name="others" class="form-control" placeholder="Others" autocomplete="off">
                        </div>
                        <div class="at-btnarea">
                            <a id="at-btnaddcarddetailvtwo" class="at-btn" href="javascript:void(0);">Next</a>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div id="at-addcarddetailtwo" class="at-contentholder at-addcarddetailtwo">
                <div class="at-contenthead">
                    <a id="at-btnback" href="javascript:void(0);" class="at-btnback"><i class="icon-arrow-left"></i></a>
                    <h2>Add Card</h2>
                </div>
                <div class="at-contentbox">
                    <fieldset>
                        <div id="at-accordion" class="at-accordion" role="tablist" aria-multiselectable="true">
                            <div class="at-panel">
                                <h4>Checklist <i class="fa fa-angle-right"></i></h4>
                                <div class="at-panelcontent">
                                    <div class="at-checkboxes">
                                        @forelse($checkLists as $checkList)
                                            <span class="at-checkbox">
                                                <input type="checkbox" name="checklist[]" id="at-checktwo{{$checkList->id}}" value="{{$checkList->id}}">
                                                <label for="at-checktwo{{$checkList->id}}">{{$checkList->name}}</label>
                                            </span>
                                        @empty
                                        @endforelse
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="at-addmore at-addnewlist">
                                        <div class="form-group">
                                            <a href="javascript:void(0)" id="checkListBtn"><i class="fa fa-paper-plane"></i></a>
                                            <input  class="form-control"  placeholder="Add New" id="checkListInput">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="at-panel">
                                <h4>Comments<i class="fa fa-angle-right"></i></h4>
                                <div class="at-panelcontent">
                                    <div class="at-description">
                                        <textarea  name="comment"></textarea>
                                    </div>
                                    <div class="clearfix"></div>

                                </div>
                            </div>
                        </div>
                        <div class="at-btnarea">
                            <a id="cardButton"  class="at-btn" >submit</a>
                        </div>
                    </fieldset>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $('#checkListBtn').on('click',function () {
                var name = $('#checkListInput').val();
                $.ajax({
                    type : 'POST',
                    url : "{{route('submit-checkList')}}",
                    data : {
                        'name' : name,
                        '_token' : '{{csrf_token()}}'
                    },
                    success : function (response) {
                        var element = '<span class="at-checkbox">\n' +
                            '<input type="checkbox" name="checklist[]" id="at-checktwo'+response.id+'" value="'+response.id+'">\n' +
                            '<label for="at-checktwo'+response.id+'">'+response.name+'</label>\n' +
                            '</span>';
                        $('.at-checkboxes').append(element);
                        $('#checkListInput').val('');

                    }



                })

            });

            $('#cardButton').on('click',function (e) {
                e.preventDefault();
                debugger;
                var title = $('#title   ').val();

                var text = tinyMCE.activeEditor.getContent();

                if (title == '' || text == ''){
                    alert('Title and Description is required, Please fill before submit!');
                }else{
                    $('#card_form').submit();
                }
            })

        })
    </script>
@endsection