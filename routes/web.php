<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('sign-up','SignUpController@signUp')->name('sign-up');
Route::post('user-register','SignUpController@userRegister')->name('user-register');




Route::group(['middleware' => 'auth'],function (){
    Route::get('logout','\App\Http\Controllers\Auth\LoginController@logout');
    Route::get('edit-profile/{id}','User\ProfileController@editProfile')->name('edit-profile');
    Route::put('update-profile/{id}','User\ProfileController@updateProfile')->name('update-profile');

    Route::get('add-card','User\CardController@addCard')->name('add-card');
    Route::post('submit-card','User\CardController@submitCard')->name('submit-card');
    Route::get('edit-card/{id}','User\CardController@editCard')->name('edit-card');
    Route::put('update-card/{id}','User\CardController@updateCard')->name('update-card');
    Route::delete('delete-card/{id}','User\CardController@deleteCard')->name('delete-card');


    Route::post('submit/check-list','User\CheckListController@submitCheckList')->name('submit-checkList');

    Route::get('/calendar','User\CalendarController@dashboardCalendar')->name('calendar');

    Route::get('/upload-file','User\FileController@uploadFile')->name('upload-file');
    Route::post('submit-file','User\FileController@submitFile')->name('submit-file');

});
Route::group(['middleware' => ['auth','admin'],'prefix' => 'admin','namespace' => 'Admin'],function (){
    Route::get('/add-organization','OrganizationController@addOrganization');
    Route::POST('/submit-organization','OrganizationController@submitOrganization')->name('submit-organization');
    Route::get('edit-organization/{id}','OrganizationController@editOrganization')->name('edit-organization');
    Route::put('update-organization/{id}','OrganizationController@updateOrganization')->name('update-organization');
    Route::delete('delete-organization/{id}','OrganizationController@deleteOrganization')->name('delete-organization');


    Route::get('/add-job','JobController@addJob');
    Route::POST('/submit-job','JobController@submitJob')->name('submit-job');
    Route::get('edit-job/{id}','JobController@editJob')->name('edit-job');
    Route::put('update-job/{id}','JobController@updateJob')->name('update-job');
    Route::delete('delete-job/{id}','JobController@deleteJob')->name('delete-job');

    Route::get('/users','UserController@users')->name('user');

});


