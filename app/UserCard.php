<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCard extends Model
{
    protected $guarded=[];

    public function checkLists()
    {
        return $this->belongsToMany(CheckList::class,'card_check_list','card_id','check_list_id');
    }

    public function files()
    {
        return $this->hasMany(CardFile::class,'card_id','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
