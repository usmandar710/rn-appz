<?php

namespace App\Http\Controllers;

use App\UserCard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

        $user = Auth::user();

        $q2= $user->cards();

        if ($request->has('title') && $request->get('title') != ""){
            $q2 = $q2->where('title','Like','%'.$request->get('title').'%');
        }

         $cards =$q2->get();

        return view('users.user-dashboard',compact('cards'));
    }
}
