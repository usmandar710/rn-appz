<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserSignUpRequest;
use App\Job;
use App\Organization;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SignUpController extends Controller
{
    public function signUp()
    {
        $organizations = Organization::all();
        $jobs = Job::all();

        return view('sign-up',compact('organizations','jobs'));
    }

    public function userRegister(UserSignUpRequest $request)
    {

       $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'contact' => $request->get('contact'),
            'organization_id' => $request->get('organization'),
            'job_id' => $request->get('job'),
            'role_id' => 2,
            'dob' => $request->get('dob'),
            'password' => bcrypt($request->get('password')),
        ]);

       Auth::login($user);
       return redirect('/home')->with('success','You account has been register successfully');
    }
}
