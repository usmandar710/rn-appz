<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CalendarController extends Controller
{
    public function dashboardCalendar()
    {
        return view('users.calendar.dashboard-calender');
    }
}
