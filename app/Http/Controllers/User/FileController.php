<?php

namespace App\Http\Controllers\User;

use App\CardFile;
use App\UserFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FileController extends Controller
{
    public function uploadFile()
    {
        $organId = Auth::user()->organization_id;
        $files = UserFile::where('organization_id',$organId)->latest()->get();

        return view('users.file.upload-file',compact('files'));
    }

    public function submitFile(Request $request)
    {
        if($files=$request->file('file')){
            foreach($files as $file){
                $name  = time().'.'.$file->getClientOriginalExtension();
                $file->move('user-files',$name);

                UserFile::create([
                    'user_id' => Auth::user()->id,
                    'file' => $name,
                    'organization_id' => Auth::user()->organization_id,
                ]);
            }
        }

        return redirect()->back();
    }
}
