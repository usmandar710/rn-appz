<?php

namespace App\Http\Controllers\User;

use App\CardFile;
use App\CheckList;
use App\User;
use App\UserCard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CardController extends Controller
{
    public function addCard()
    {
        $users = User::where('organization_id',Auth::user()->organization_id)->where('id','!=',Auth::user()->id)->get();
        $checkLists = CheckList::where('user_id',Auth::user()->id)->latest()->get();

        return view('users.card.add-card',compact('users','checkLists'));
    }

    public function submitCard(Request $request)
    {
        $name =null;

        $card = UserCard::create([
            'title' => $request->get('title'),
            'text' => $request->get('text'),
            'user_id' => Auth::user()->id,
            'photo_id' => $name,
            'surgeon_name' => $request->get('surgeon-name'),
            'procedure_type' => $request->get('procedure-type'),
            'special_need' => $request->get('special-needs'),
            'suplies' => $request->get('suplies'),
            'drug' => $request->get('drugs'),
            'equipment' => $request->get('equipment'),
            'instruments' => $request->get('instruments'),
            'nursing_Instruction' => $request->get('nursing-instructions'),
            'positioning' => $request->get('positioning'),
            'scrub_hint' => $request->get('scrub-hints'),
            'others' => $request->get('others'),
            'comment' => $request->get('comment'),
        ]);

       $card->checkLists()->sync($request->checklist);

        if($files=$request->file('photo_id')){
            foreach($files as $file){
                $name  = time().'.'.$file->getClientOriginalExtension();
                $file->move('card-images',$name);

                CardFile::create([
                    'card_id' => $card->id,
                    'file' => $name,
                ]);
            }
        }

       return redirect('/home');
    }

    public function editCard($id)
    {
        $card = UserCard::findOrFail($id);
        $checkLists = CheckList::where('user_id',Auth::user()->id)->latest()->get();

        $cardCheckList = $card->checkLists;

        return view('users.card.edit-card',compact('cardCheckList','card','checkLists'));
    }

    public function updateCard(Request $request,$id)
    {
        $name =null;

//        dd($request->all());
        $card = UserCard::findOrFail($id);

        $card->update([
            'title' => $request->get('title'),
            'text' => $request->get('text'),
            'surgeon_name' => $request->get('surgeon-name'),
            'procedure_type' => $request->get('procedure-type'),
            'special_need' => $request->get('special-needs'),
            'suplies' => $request->get('suplies'),
            'drug' => $request->get('drugs'),
            'equipment' => $request->get('equipment'),
            'instruments' => $request->get('instruments'),
            'nursing_Instruction' => $request->get('nursing-instructions'),
            'positioning' => $request->get('positioning'),
            'scrub_hint' => $request->get('scrub-hints'),
            'others' => $request->get('others'),
            'comment' => $request->get('comment'),
        ]);

        $card->checkLists()->sync($request->checklist);

        if($files=$request->file('file')){
            foreach($files as $file){
                $name  = time().'.'.$file->getClientOriginalExtension();
                $file->move('card-images',$name);

                CardFile::create([
                    'card_id' => $card->id,
                    'file' => $name,
                ]);
            }
        }


        return redirect('/home');

    }

    public function deleteCard($id)
    {
        $card = UserCard::findOrFail($id);

        $card->delete();

        return redirect('/home');
    }
}
