<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\UserUpdateRequest;
use App\Job;
use App\Organization;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function editProfile($id)
    {
        $organizations = Organization::all();
        $jobs = Job::all();
        $user = User::findOrFail($id);

        return view('users.edit-profile',compact('user','organizations','jobs'));
    }

    public function updateProfile(UserUpdateRequest $request,$id)
    {
       $user = User::findOrFail($id);

       if (!empty($request->get('password'))){
           $user->password = bcrypt($request->get('password'));
       }

       $user->update([
           'name' => $request->get('name'),
           'email' => $request->get('email'),
           'contact' => $request->get('contact'),
           'organization_id' => $request->get('organization'),
           'dob' => $request->get('dob'),
       ]);

       return redirect('/home');
    }
}
