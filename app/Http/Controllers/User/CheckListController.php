<?php

namespace App\Http\Controllers\User;

use App\CheckList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CheckListController extends Controller
{
    public function submitCheckList(Request $request)
    {
        return CheckList::create([
            'name' => $request->get('name'),
            'user_id' =>Auth::user()->id
        ]);
    }
}
