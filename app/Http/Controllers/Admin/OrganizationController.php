<?php

namespace App\Http\Controllers\Admin;

use App\Organization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrganizationController extends Controller
{
    public function addOrganization()
    {
        $organizations = Organization::all();
        return view('admin.organization.add-organization',compact('organizations'));
    }

    public function submitOrganization(Request $request)
    {
      $data =  Organization::create([
            'name' => $request->get('name')
        ]);
      $sr = Organization::count();
        return view('admin.organization.new-organization',compact('data','sr'));
    }

    public function editOrganization($id)
    {
        $organ = Organization::findOrFail($id);

        return view('admin.organization.edit-organization',compact('organ'));
    }

    public function updateOrganization(Request $request,$id)
    {
        $organ = Organization::findOrFail($id);

        $organ->update([
            'name' => $request->get('name')
        ]);
        return redirect('admin/add-organization');
    }

    public function deleteOrganization($id)
    {
        $organ = Organization::findOrFail($id);

        $organ->delete();
        return redirect('admin/add-organization');
    }
}
