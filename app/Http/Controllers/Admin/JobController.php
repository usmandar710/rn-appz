<?php

namespace App\Http\Controllers\Admin;

use App\Job;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobController extends Controller
{
    public function addJob()
    {
        $jobs = Job::all();
        return view('admin.job.add-job',compact('jobs'));
    }

    public function submitJob(Request $request)
    {
        $data =  Job::create([
            'name' => $request->get('name')
        ]);
        $sr=Job::count();

        return view('admin.job.new-jobs',compact('data','sr'));
    }

    public function editJob($id)
    {
        $job = Job::findOrFail($id);

        return view('admin.job.edit-job',compact('job'));
    }

    public function updateJob(Request $request,$id)
    {
        $organ = Job::findOrFail($id);

        $organ->update([
            'name' => $request->get('name')
        ]);
        return redirect('admin/add-job');
    }

    public function deleteJob($id)
    {
        $organ = Job::findOrFail($id);

        $organ->delete();
        return redirect('admin/add-job');
    }
}
